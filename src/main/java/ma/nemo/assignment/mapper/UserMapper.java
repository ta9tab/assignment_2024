package ma.nemo.assignment.mapper;
import ma.nemo.assignment.domain.User;
import ma.nemo.assignment.dto.UserDto;
import java.util.List;

public class UserMapper {
    public static UserDto map(User user) {
        UserDto userDto = new UserDto();
        userDto.setUserId(user.getUserId());
        userDto.setUsername(user.getUsername());
        userDto.setRole(user.getRole());
        return userDto;
    }

    public static User map(UserDto userDto) {
        User user = new User();
        user.setUserId(userDto.getUserId());
        user.setUsername(userDto.getUsername());
        user.setRole(userDto.getRole());
        return user;
    }

    public static List<UserDto> mapList(List<User> users) {
        return users.stream().map(UserMapper::map).toList();
    }

    public static List<User> mapListDto(List<UserDto> userDtos) {
        return userDtos.stream().map(UserMapper::map).toList();
    }
}