package ma.nemo.assignment.mapper;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.dto.ProductDto;

import java.util.List;

public class ProductMapper {


    public static ProductDto map(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setProductId(product.getProductId());
        productDto.setProductName(product.getProductName());
        productDto.setProductCode(product.getProductCode());
        productDto.setDescription(product.getDescription());
        productDto.setUnitPrice(product.getUnitPrice());
        productDto.setQuantityInStock(product.getQuantityInStock());
        productDto.setCreationDate(product.getCreationDate());
        productDto.setDescription(product.getDescription());

        return productDto;
    } 
    public static Product map(ProductDto productDto) {
        Product product= new Product();
        product.setProductId(productDto.getProductId());
        product.setProductName(productDto.getProductName());
        product.setProductCode(productDto.getProductCode());
        product.setDescription(productDto.getDescription());
        product.setUnitPrice(productDto.getUnitPrice());
        product.setQuantityInStock(productDto.getQuantityInStock());
        product.setCreationDate(productDto.getCreationDate());
        product.setDescription(productDto.getDescription());

        return product;

    }
    public static List<ProductDto> mapList(List<Product> products){
        return products.stream().map(ProductMapper::map).toList();
    }
    public static List<Product> mapListDto(List<ProductDto> productDtos){
        return productDtos.stream().map(ProductMapper::map).toList();
    }


}
