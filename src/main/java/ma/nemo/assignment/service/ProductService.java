package ma.nemo.assignment.service;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.dto.ProductDto;
import ma.nemo.assignment.exceptions.ProductAlreadyExists;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.exceptions.ProductValidationException;
import ma.nemo.assignment.mapper.ProductMapper;
import ma.nemo.assignment.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Service
public class ProductService {
    Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductDto createProduct(ProductDto productDto) throws ProductAlreadyExists, ProductValidationException {

        LOGGER.info("Creating prd {}", productDto);

        Product p = productRepository.findByProductCode(productDto.getProductCode());

        if (p != null) {
           LOGGER.error("Product with code {} already exists", productDto.getProductCode());
            throw new ProductAlreadyExists();
        }

        if (productDto.getProductCode() == null) {
           LOGGER.error("Product code is null");
            throw new ProductValidationException("Product code is null");
        } else if (productDto.getProductCode().equals("")) {
           LOGGER.error("Product code is empty");
            throw new ProductValidationException("Product code is empty");
        } else if (productDto.getProductCode().length() > 10) {
           LOGGER.error("Product code is too long");
            throw new ProductValidationException("Product code is too long");
        } else if (productDto.getProductCode().length() < 3) {
           LOGGER.error("Product code is too short");
            throw new ProductValidationException("Product code is too short");
        } else if (productDto.getQuantityInStock() <= 0) {
           LOGGER.error("Product quantity is invalid");
            throw new ProductValidationException("Product quantity is invalid");
        } else if (productDto.getUnitPrice().intValue() < 0) {
           LOGGER.error("Product price is invalid");
            throw new ProductValidationException("Product price is invalid");
        }
        Product product = ProductMapper.map(productDto);
        product.setCreationDate(new Date());
        product.setModificationDate(new Date());

        Product saved = productRepository.save(product);
        return ProductMapper.map(saved);
    }

    public  List<ProductDto>  getAllProducts(){
        LOGGER.info("Listing products");
        List<Product> prds = productRepository.findAll();

        if (CollectionUtils.isEmpty(prds)) {
            return null;
        } else {
            return ProductMapper.mapList(prds);
        }
    }
    public ProductDto getProductDto(long id ) throws ProductNotFound {
        Optional<Product> optionalProduct = productRepository.findById(id);
        return ProductMapper.map(optionalProduct.orElseThrow(ProductNotFound::new));

    }

   public  void deleteProduct(long id){

           productRepository.deleteById(id);

   }


}
