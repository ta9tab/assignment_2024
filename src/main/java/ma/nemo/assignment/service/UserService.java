package ma.nemo.assignment.service;
import ma.nemo.assignment.domain.User;
import ma.nemo.assignment.dto.UserDto;
import ma.nemo.assignment.exceptions.UserNotFound;
import ma.nemo.assignment.mapper.UserMapper;
import ma.nemo.assignment.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDto createUser(UserDto userDto){
        User user = UserMapper.map(userDto);
        User saved = userRepository.save(user);
        return UserMapper.map(saved);
    }


    public UserDto getUserDto(long id ) throws UserNotFound {
        Optional<User> optionalUser = userRepository.findById(id);
        return UserMapper.map(optionalUser.orElseThrow(UserNotFound::new));
    }

    public  void deleteUser(long id){

        userRepository.deleteById(id);
    }

    public List<UserDto> getAllUsers(){
        LOGGER.info("Listing users");
        List<User> users = userRepository.findAll();

        if (CollectionUtils.isEmpty(users)) {
            return null;
        } else {
            return UserMapper.mapList(users);
        }
    }
}
