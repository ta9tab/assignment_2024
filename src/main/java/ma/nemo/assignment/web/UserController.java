package ma.nemo.assignment.web;

import ma.nemo.assignment.dto.UserDto;
import ma.nemo.assignment.exceptions.UserNotFound;
import ma.nemo.assignment.repository.UserRepository;
import ma.nemo.assignment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public Long createUser(@RequestBody UserDto user) {
        return userService.createUser(user).getUserId();
    }

    @GetMapping
    public List<UserDto> listUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable Long id) throws UserNotFound {
        return userService.getUserDto(id);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }
}
